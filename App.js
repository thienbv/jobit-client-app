import React, {useState} from 'react';
import 'react-native-gesture-handler';
import {cacheImages, cacheFonts} from './src/utils/AssetsCaching';
import vectorFonts from './src/utils/vector-fonts';
import * as SplashScreen from 'expo-splash-screen';
import {AppLoading} from 'expo';

import MainNavigator from './src/navigations/MainNavigator';
import {NavigationContainer} from '@react-navigation/native';
import {Provider as JobProvider} from './src/contexts/JobContext';

export default () => {
  const [isReady, setIsReady] = useState(false);
  /*
  const _cacheResourcesAsync = async() => {
    const images = [require('./assets/snack-icon.png')];

    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    }); 
    return Promise.all(cacheImages);
  }
  */
  const loadAssetsAsync = async () => {
    await SplashScreen.preventAutoHideAsync();
    // const imageAssets = cacheImages([require('./assets/cups.png')]);

    const fontAssets = cacheFonts([
      ...vectorFonts,
      {'Muli-Light': require('./assets/fonts/Muli-Light.ttf')},
      {'Muli-Regular': require('./assets/fonts/Muli-Regular.ttf')},
      {'Muli-Medium': require('./assets/fonts/Muli-Medium.ttf')},
      {'Muli-ExtraBold': require('./assets/fonts/Muli-ExtraBold.ttf')},
      {'Muli-Bold': require('./assets/fonts/Muli-Bold.ttf')},
      {'Muli-SemiBold': require('./assets/fonts/Muli-SemiBold.ttf')},
    ]);

    await Promise.all([...fontAssets]);
  };
  if (!isReady) {
    return (
      <AppLoading
        startAsync={loadAssetsAsync}
        onFinish={() => setIsReady(true)}
        onError={console.warn}
      />
    );
  }
  return (
    <JobProvider>
      <NavigationContainer>
        <MainNavigator />
      </NavigationContainer>
    </JobProvider>
  );
};
