import createDataContext from './createDataContext';
import jobitService from '../api/jobitService';
import API from '../constants/api';

const ACTIONS = {
  GET_ALL: 'GET_ALL',
  ADD_ERROR_MESSAGE: 'ADD_ERROR_MESSAGE',
};

const jobReducer = (state, action) => {
  switch (action.type) {
    case ACTIONS.GET_ALL:
      return {...state, jobs: action.payload, success: true, isLoading: false};
    default:
      return {...state};
  }
};

const getAllJobs = dispatch => async () => {
  try {
    const res = await jobitService.get(API.API_JOB_GET_ALL);
    dispatch({type: ACTIONS.GET_ALL, payload: res.data.jobs});
  } catch (e) {
    console.log(e);
    addErrorMessage({
      type: ACTIONS.ADD_ERROR_MESSAGE,
      message: e.response.data.message,
    });
  }
};
const initLoading = dispatch => {
  dispatch({type: AuthActionType.INIT_LOADING});
};

const checkSessionExpired = async ({dispatch, e}) => {
  if (e.response.status == 401) {
    Alert.alert('Session expired please login again!');
    await removeItem(Global.TOKEN);
    await removeItem(Global.USER);
    dispatch({type: AuthActionType.LOGOUT});
    dispatch({
      type: AuthActionType.ADD_ERROR_AUTH,
      payload: e.response.data.messages || e.response.data.message,
    });
  } else {
    console.log(e.response.data);
    dispatch({
      type: AuthActionType.ADD_ERROR_AUTH,
      payload: e.response.data.messages || e.response.data.message,
    });
  }
};

const clearErrorMessage = dispatch => () => {
  dispatch({type: AuthActionType.CLEAR_ERROR_MESSAGE});
};

const addErrorMessage = dispatch => message => {
  dispatch({type: AuthActionType.ADD_ERROR_MESSAGE, payload: message});
};

export const {Provider, Context} = createDataContext(
  jobReducer,
  {
    getAllJobs,
    clearErrorMessage,
    addErrorMessage,
  },
  {errorMessage: '', isLoading: true, success: false}
);
