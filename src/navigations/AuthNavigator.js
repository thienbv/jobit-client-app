import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Routes from '../constants/Routes';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
const Stack = createStackNavigator ();
const AuthNavigator = () => {
    return (
        <Stack.Navigator screenOptions={{headerShown:false}}>
          <Stack.Screen name={Routes.LOGIN} component={LoginScreen} />
          <Stack.Screen name={Routes.REGISTER} component={RegisterScreen} />
        </Stack.Navigator>
      );
}

export default AuthNavigator
