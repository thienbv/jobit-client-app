import React from 'react'
import Routes from '../constants/Routes';
import { DrawerContent } from  '../screens/DrawerContent';
import {createDrawerNavigator} from '@react-navigation/drawer';
import TopCompanyScreen from '../screens/TopCompanyScreen';
import AuthNavigator from '../navigations/AuthNavigator'
import HomeNavigator from './HomeNavigator';
import AboutScreen from '../screens/AboutScreen';
const Drawer = createDrawerNavigator();
const MainNavigator = ({navigation}) => {
    return (
    <Drawer.Navigator
      drawerContent={props => <DrawerContent {...props} />}
      initialRouteName={Routes.HOME_NAV}
    >
      <Drawer.Screen name={Routes.HOME_NAV}  component={HomeNavigator} />
      <Drawer.Screen
        name={Routes.TOP_COMPANY}
        component={TopCompanyScreen}
      />
      <Drawer.Screen name={Routes.AUTH_NAV} component={AuthNavigator} />
      <Drawer.Screen name={Routes.ABOUT} component={AboutScreen} />
    </Drawer.Navigator>
    )
}

export default MainNavigator
