import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Routes from '../constants/Routes';
import HomeScreen from '../screens/HomeScreen';
import JobDetailScreen from '../screens/JobDetailScreen';
const Stack = createStackNavigator ();
const HomeNavigator = () => {
    return (
        <Stack.Navigator screenOptions={{headerShown:false}}>
          <Stack.Screen name={Routes.HOME} component={HomeScreen} />
          <Stack.Screen name={Routes.JOB_DETAIL} component={JobDetailScreen} />
        </Stack.Navigator>
      );
}

export default HomeNavigator
