import React from 'react'
import {
    Container,
    Header,
    Left,
    Body,
    Right,
    Button,
    Icon,
    Title,
    Content,
    Text,
    Thumbnail,
  } from 'native-base';
  import colors from '../constants/colors';
  import {View} from 'react-native';
import Routes from '../constants/Routes';
const AboutScreen = ({navigation}) => {
    return (
        <Container>
        <Header style={{backgroundColor: colors.main}}>
          <Left>
            <Button transparent onPress={() => navigation.navigate(Routes.HOME_NAV)}>
              <Icon  name='back' type="AntDesign" style={{color: colors.primary, fontSize: 25}} fontSize={20}/>
            </Button>
          </Left>
          <Body>
            <Title style={{color: "#fff", fontFamily:'Muli-Bold'}}>About</Title>
          </Body>
          <Right>
          </Right>
        </Header>
      </Container>
    )
}

export default AboutScreen
