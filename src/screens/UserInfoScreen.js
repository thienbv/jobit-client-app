import React from 'react'
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
const UserInfoScreen = () => {
    return (
        <Container>
        <Header style={{backgroundColor: colors.main}}>
          <Left>
            <Button transparent>
              <Icon  name='search1' type="AntDesign" style={{color: colors.primary, fontSize: 25}} fontSize={20}/>
            </Button>
          </Left>
          <Body>
            <Title style={{color: "#fff", fontFamily:'Muli-Bold'}}>User Info</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon  name='menu-fold' type="AntDesign" style={{color: colors.primary, fontSize: 25}} fontSize={20}/>
            </Button>
          </Right>
        </Header>
      </Container>
    )
}

export default UserInfoScreen
