import React from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Content,
  Text,
  Thumbnail,
} from 'native-base';
import colors from '../constants/colors';
import {View} from 'react-native';
import {useRoute} from '@react-navigation/native';
import Moment from 'moment';
const JobDetailScreen = ({navigation}) => {
  const route = useRoute();
  const {job} = route.params;
  console.log(job);
  return (
    <Container>
      <Header style={{backgroundColor: colors.main}}>
        <Left>
          <Button transparent onPress={() => navigation.goBack()}>
            <Icon
              name="back"
              type="AntDesign"
              style={{color: colors.primary, fontSize: 25}}
              fontSize={20}
            />
          </Button>
        </Left>
        <Body>
          <Title style={{color: '#fff', fontFamily: 'Muli-Bold'}}>
            Job Detail
          </Title>
        </Body>
        <Right></Right>
      </Header>
      <View
        style={{
          justifyContent: 'flex-start',
          flexDirection: 'row',
          alignItems: 'center',
          marginVertical: 10,
          marginHorizontal: 10,
        }}
      >
        <Thumbnail
          large
          circle
          source={{uri: job.avatar}}
          resizeMode="contain"
          style={{borderWidth: 1, borderColor: colors.primary}}
        />
        <View style={{marginHorizontal: 10}}>
          <Text
            style={{
              textTransform: 'uppercase',
              fontFamily: 'Muli-Bold',
              fontSize: 14,
              color: colors.primary,
            }}
          >
            {job.user_name}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}
          >
            <Icon name="smartphone" type="Feather" style={{fontSize: 18}} />
            <Text
              style={{
                textTransform: 'uppercase',
                fontFamily: 'Muli-Medium',
                fontSize: 14,
              }}
            >
              {job.phone}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}
          >
            <Icon
              name="email-outline"
              type="MaterialCommunityIcons"
              style={{fontSize: 18}}
            />
            <Text style={{fontFamily: 'Muli-Medium'}}>{job.email}</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}
          >
            <Icon name="location" type="EvilIcons" style={{fontSize: 18}} />
            <Text style={{fontFamily: 'Muli-Medium'}}>{job.address}</Text>
          </View>
        </View>
      </View>
      <Content padder>
        <View>
          <Text style={{fontFamily: 'Muli-Bold', marginVertical: 5}}>
            {job.title}
          </Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon name="location" type="EvilIcons" style={{fontSize: 20}} />
            <Text style={{fontFamily: 'Muli-Regular', marginLeft: 5}}>
              {job.location}
            </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon
              type="MaterialIcons"
              name="attach-money"
              style={{fontSize: 20}}
            />
            <Text style={{fontFamily: 'Muli-Regular', marginLeft: 5}}>
              {(+job.salary_min).toLocaleString()} - {(+job.salary_max).toLocaleString()} {job.salary_type}
            </Text>
          </View>

          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon type="Feather" name="users" style={{fontSize: 20}} />
            <Text style={{fontFamily: 'Muli-Regular', marginLeft: 5, textTransform:'capitalize'}}>
              {job.positions}
            </Text>
          </View>
          <Text style={{fontFamily: 'Muli-Regular', marginTop: 10}}>{job.excerpt}</Text>
          <Button style={{justifyContent:'center', alignItems:'center', backgroundColor:colors.main, marginVertical: 20}}>
            <Text style={{color:colors.primary}}>Apply Job</Text>
          </Button>
        </View>
      </Content>
    </Container>
  );
};

export default JobDetailScreen;
