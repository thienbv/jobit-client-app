import React, {useContext, useEffect} from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  List,
  ListItem,
  Content,
  Thumbnail,
  Text,
} from 'native-base';
import colors from '../constants/colors';
import {Context as JobContext} from '../contexts/JobContext';
import Moment from 'moment';
import {View} from 'react-native';
import Routes from '../constants/Routes';
import {ActivityIndicator} from 'react-native-paper';

const HomeScreen = ({navigation}) => {
  const {
    state: {jobs, isLoading},
    getAllJobs,
  } = useContext(JobContext);

  useEffect(() => {
    getAllJobs();
  }, []);

  const openMenu = () => {
    navigation.openDrawer();
  };
  const RenderJob = ({job}) => {
    return (
      <ListItem
        thumbnail
        onPress={() => navigation.navigate(Routes.JOB_DETAIL, {job})}
      >
        <Left style={{flexDirection: 'column'}}>
          <Thumbnail circle resizeMode="contain" source={{uri: job.avatar}} />
          <Text
            style={{fontFamily: 'Muli-Regular', fontSize: 15}}
            numberOfLines={1}
          >
            {job.user_name}
          </Text>
        </Left>
        <Body>
          <Text numberOfLines={2} style={{fontFamily: 'Muli-Medium'}}>
            {job.title}
          </Text>
          <Text
            note
            numberOfLines={5}
            style={{fontFamily: 'Muli-Regular', marginVertical: 5}}
          >
            {job.excerpt}
          </Text>
          <Text style={{fontFamily: 'Muli-Regular', fontSize: 14}}>
            {Moment(job.start_find).format('YYYY/MM/DD')} -{' '}
            {Moment(job.expired_find).format('YYYY/MM/DD')}
          </Text>
        </Body>
      </ListItem>
    );
  };
  return (
    <Container>
      <Header style={{backgroundColor: colors.main}}>
        <Left>
          <Button transparent>
            <Icon
              name="search1"
              type="AntDesign"
              style={{color: colors.primary, fontSize: 25}}
              fontSize={20}
            />
          </Button>
        </Left>
        <Body>
          <Title style={{color: '#fff', fontFamily: 'Muli-Bold'}}>JOB-IT</Title>
        </Body>
        <Right>
          <Button transparent onPress={openMenu}>
            <Icon
              name="menu-fold"
              type="AntDesign"
              style={{color: colors.primary, fontSize: 25}}
              fontSize={20}
            />
          </Button>
        </Right>
      </Header>
      {jobs ? (
        <List
          keyExtractor={(item, _) => _.toString()}
          dataArray={jobs}
          renderRow={(item, index) => <RenderJob key={index} job={item} />}
        />
      ) : (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size={30} color={colors.main} />
        </View>
      )}
    </Container>
  );
};

export default HomeScreen;
