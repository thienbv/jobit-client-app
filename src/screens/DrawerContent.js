import React, {useContext, useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Drawer, TouchableRipple} from 'react-native-paper';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import Routes from '../constants/Routes';
import colors from '../constants/colors';
import {Icon} from 'native-base';

export const DrawerContent = props => {
  return (
    <View style={{flex: 1, backgroundColor: colors.main}}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <Drawer.Section style={styles.drawerSection}>
            <DrawerItem
              label="Home"
              style={styles.itemMenu}
              labelStyle={styles.labelMenu}
              onPress={() => {
                props.navigation.navigate(Routes.HOME);
              }}
              icon={({focused, color, size}) => (
                <Icon
                  name="home"
                  type="AntDesign"
                  style={{color: colors.primary, fontSize: 20}}
                />
              )}
            />
            <DrawerItem
              label="Top Company"
              style={styles.itemMenu}
              labelStyle={styles.labelMenu}
              onPress={() => {
                props.navigation.navigate(Routes.TOP_COMPANY);
              }}
              icon={({focused, color, size}) => (
                <Icon
                  name="like1"
                  type="AntDesign"
                  style={{color: colors.primary, fontSize: 20}}
                />
              )}
            />
            <DrawerItem
              label="About JOBIT"
              style={styles.itemMenu}
              labelStyle={styles.labelMenu}
              onPress={() => {
                props.navigation.navigate(Routes.ABOUT);
              }}
              icon={({focused, color, size}) => (
                <Icon
                  name="idcard"
                  type="AntDesign"
                  style={{color: colors.primary, fontSize: 20}}
                />
              )}
            />
            <DrawerItem
              label="Login"
              style={styles.itemMenu}
              labelStyle={styles.labelMenu}
              onPress={() => {
                props.navigation.navigate(Routes.AUTH_NAV);
              }}
              icon={({focused, color, size}) => (
                <Icon
                  name="key"
                  type="AntDesign"
                  style={{color: colors.primary, fontSize: 20}}
                />
              )}
            />
          </Drawer.Section>
          <View style={{marginLeft: 20}}>
            <Text style={{color: colors.primary}}>Version 1.0.0</Text>
          </View>
        </View>
      </DrawerContentScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  labelMenu: {
    color: colors.primary,
    fontFamily: 'Muli-Medium',
    fontSize: 16,
  },
  itemMenu: {
    borderBottomWidth: 0.3,
    borderBottomColor: colors.primary,
    justifyContent: 'center',
  },
});
