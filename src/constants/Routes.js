export default {
  //AUTH
  AUTH_NAV: 'AUTH_NAV',
  LOGIN: 'LOGIN',
  REGISTER: 'REGISTER',
  LOGOUT: 'LOGOUT',
  FORGOT_PASSWORD: 'FORGOT_PASSWORD',
  MAIN_NAV: 'MAIN_NAV',
  
  //HOME
  HOME_NAV: 'HOME_NAV',
  HOME: 'HOME',
  ABOUT: 'ABOUT',

  TOP_COMPANY: 'TOP_COMPANY',

  LIST_JOB: 'LIST_JOB',
  JOB_DETAIL: 'JOB_DETAIL',
};
