export default {
  /**
   * JOB API
   */

  //Get all job
  API_JOB_GET_ALL: '/job/getAll',
  //Get job by yser
  API_JOB_GET_BY_USER: '/job/getByUser',
  //Search job by option
  API_JOB_SEARCH: '/job/search/?option=:option&query=:query',
  //Update job
  API_JOB_UPDATE: '/job/update/:job_id',
  //Create job
  API_JOB_CREATE: '/job/create',
  API_JOB_DETAIL: '/job/detail/:job_id',

  /**
   * USER API
   */
  API_USER_LOGIN: '/user/login',
  API_USER_APPLY: '/user/apply',
  API_USER_CREATE: '/user/create',
  API_USER_VOTE: '/user/vote',
  API_USER_REPORT: '/user/report',
  API_USER_UPDATE_VOTE: '/user/vote/update/:vote_id',
  API_USER_UPDATE_REPORT: '/user/vote/update/:report_id',

  /**
   * CANDIDATE API
   */
  API_CANDIDATE_CREATE: '/candidate/create',
  API_CANDIDATE_UPDATE: '/candidate/update/:cand_id',
  API_CANDIDATE_GET_ALL: '/candidate/getAll',
  API_CANDIDATE_GET_BY_USER: '/candidate/getByUser',

   /**
   * CV API
   */
   API_CV_CREATE:'/cv/create',
   API_CV_UPDATE:'/cv/update',
   API_CV_GET_BY_USER_ID_COMPANY:'/cv/user/:user_id',
   API_CV_GET_BY_USER_ID:'/cv/user',
   API_CV_GET_BY_OPTION:'/cv/search/?option=:option&query=:query',
};
