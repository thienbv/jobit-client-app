import constants from '../constants/constants';
import {AsyncStorage} from 'react-native';

export const getItem = async key => {
  try {
    const value = await AsyncStorage.getItem(`${constants.APP_NAME}_${key}`);
    if (value !== null) {
      return value;
    }
  } catch (error) {
    console.log(`ERROR: ${error}`);
    return null;
  
  }
};
export const setItem = async (key, value) => {
  try {
    await AsyncStorage.setItem(`${constants.APP_NAME}_${key}`, value);
  } catch (error) {
    console.log(`ERROR: ${error}`);
  }
};
export const removeItem = async key => {
  try {
    await AsyncStorage.removeItem(`${constants.APP_NAME}_${key}`);
  } catch (error) {
    console.log(`ERROR: ${error}`);
  }
};
