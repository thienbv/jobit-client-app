import axios from 'axios';
import constants from '../constants/constants'
import {getItem} from '../utils/StoreLocal';
const instance = axios.create({
  baseURL: constants.HOST,
});

instance.interceptors.request.use(
  async(config) => {
    const token = await getItem(constants.TOKEN);
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);
export default instance;
